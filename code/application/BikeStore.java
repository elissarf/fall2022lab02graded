//Elissar 2134398

package application;

import vehicles.*;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Merida", 24, 80);

        bikes[1] = new Bicycle("Trek", 31, 100);

        bikes[2] = new Bicycle("Specialized", 12, 60);

        bikes[3] = new Bicycle("Cannondale", 9, 35);

        int i = 1;
        for (Bicycle bike : bikes) {
            System.out.println("bike " + i);
            System.out.println(bike + "\n");
            i++;
        }
    }
}
